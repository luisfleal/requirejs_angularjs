(function(){
    'use strict';
    define(function (require){
        var angular = require('angular');
        var requireJsAngularJs = angular.module('requireJsAngularJs', []);
        requireJsAngularJs
                .controller('homeController', HomeController);
        HomeController.$inject = ['$scope'];
        function HomeController ($scope){
            var vm = this;
            vm.name = "RequireJS - AngularJS";
        };
    });
})();