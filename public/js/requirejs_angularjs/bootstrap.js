(function(){
    'use strict';
    require(['angular'], function (angular){
        require(['requireJsAngularJs'], function(){
            angular
                .element (document)
                .ready(function(){ //$('document').on('ready')
                    angular.bootstrap(document, ['requireJsAngularJs']);
                }); 
        });
    });
})();

