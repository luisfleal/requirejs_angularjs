(function(){
    'use strict';
    require.config({
        paths : {
            //General dependencies
            'angular': '../bower_components/angular/angular.min',
            
            //Local dependencies
            'requireJsAngularJs' : 'requirejs_angularjs/requirejs_angularjs.module'
        },
        
        shim:{
            'angular' : {
                'exports': 'angular'
            },
            'requireJsAngularJs' : {
                deps: ['angular']
            }
        }
    });
    require([
        'requirejs_angularjs/bootstrap'
    ]);
})();
